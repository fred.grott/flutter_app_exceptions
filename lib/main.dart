// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license.

import 'dart:async';
import 'dart:core';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_exceptions/build_modes.dart';
import 'package:flutter_app_exceptions/presentation/my_app.dart';

// In Domain Driven Development terms you would move the other
// widget presentation classes to the presentation sub-folder
Future<void> main() async {

  // since not using full app exception plugins as of yet, need 
  // an error widget to present to app user.
  ErrorWidget.builder = (FlutterErrorDetails details) {
      if (isInDebugMode){
          return ErrorWidget(details.exception);
      }
      
      return Container(
      alignment: Alignment.center,
      child: const Text(
        'Error!',
        style: TextStyle(color: Colors.yellow),
        textDirection: TextDirection.ltr,
      ),
    );

  };

  // This captures errors reported by the Flutter framework.
  FlutterError.onError = (FlutterErrorDetails details) async {
    if (isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // app exceptions provider. We do not need this in Profile mode.
      if (isInReleaseMode) {
        Zone.current.handleUncaughtError(details.exception, details.stack);
      }
    }
  };
  runZonedGuarded<Future<void>>(() async {
    runApp(MyApp());
    // ignore: prefer-trailing-comma
  }, (error, stackTrace) async {
    await _reportError(error, stackTrace);
  });
}

/// A _reportError function to report error and stacktrace to app exceptions provider
/// @author Fredrick Allan Grott
Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  log('Caught error: $error');
  // Errors thrown in development mode are unlikely to be interesting. You
  // check if you are running in dev mode using an assertion and omit send
  // the report.
  if (isInDebugMode) {
    log('$stackTrace');
    log('In dev mode. Not sending report to an app exceptions provider.');

    return;
  } else {
    // reporting error and stacktrace to app exceptions provider code goes here
    if (isInReleaseMode) {
        // code goes here
    }
  }
}
