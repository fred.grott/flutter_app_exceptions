Run your Flutter app on an attached device.

Global options:
-h, --help                  Print this usage information.
-v, --verbose               Noisy logging, including all shell commands executed.
                            If used with --help, shows hidden options.
-d, --device-id             Target device id or name (prefixes allowed).
    --version               Reports the version of this tool.
    --suppress-analytics    Suppress analytics reporting when this command runs.

Usage: flutter run [arguments]
-h, --help                                            Print this usage information.
    --debug                                           Build a debug version of your app (default mode).
    --profile                                         Build a version of your app specialized for performance profiling.
    --release                                         Build a release version of your app.
    --dart-define=<foo=bar>                           Additional key-value pairs that will be available as constants from the String.fromEnvironment, bool.fromEnvironment, int.fromEnvironment, and double.fromEnvironment constructors.
                                                      Multiple defines can be passed by repeating --dart-define multiple times.
    --flavor                                          Build a custom app flavor as defined by platform-specific build setup.
                                                      Supports the use of product flavors in Android Gradle scripts, and the use of custom Xcode schemes.
    --web-renderer                                    The renderer implementation to use when building for the web. Possible values are:
                                                      html - always use the HTML renderer. This renderer uses a combination of HTML, CSS, SVG, 2D Canvas, and WebGL. This is the default.
                                                      canvaskit - always use the CanvasKit renderer. This renderer uses WebGL and WebAssembly to render graphics.
                                                      auto - use the HTML renderer on mobile devices, and CanvasKit on desktop devices.
                                                      [auto (default), canvaskit, html]
    --trace-startup                                   Trace application startup, then exit, saving the trace to a file.
    --verbose-system-logs                             Include verbose logging from the flutter engine.
    --cache-sksl                                      Only cache the shader in SkSL instead of binary or GLSL.
    --dump-skp-on-shader-compilation                  Automatically dump the skp that triggers new shader compilations. This is useful for writing custom ShaderWarmUp to reduce jank. By default, this is not enabled to reduce the overhead. This is only available in profile or debug build. 
    --purge-persistent-cache                          Removes all existing persistent caches. This allows reproducing shader compilation jank that normally only happens the first time an app is run, or for reliable testing of compilation jank fixes (e.g. shader warm-up).
    --route                                           Which route to load when running the app.
    --vmservice-out-file=<project/example/out.txt>    A file to write the attached vmservice uri to after an application is started.
    --use-application-binary=<path/to/app.apk>        Specify a pre-built application binary to use when running. For android applications, this must be the path to an APK. For iOS applications, the path to an IPA. Other device types do not yet support prebuilt application binaries
    --[no-]start-paused                               Start in a paused mode and wait for a debugger to connect.
    --endless-trace-buffer                            Enable tracing to the endless tracer. This is useful when recording huge amounts of traces. If we need to use endless buffer to record startup traces, we can combine the ("--trace-startup"). For example, flutter run --trace-startup --endless-trace-buffer. 
    --trace-systrace                                  Enable tracing to the system tracer. This is only useful on platforms where such a tracer is available (Android and Fuchsia).
    --trace-skia                                      Enable tracing of Skia code. This is useful when debugging the raster thread (formerly known as the GPU thread). By default, Flutter will not log skia code.
-a, --dart-entrypoint-args                            Pass a list of arguments to the Dart entrypoint at application startup. By default this is main(List<String> args). Specify this option multiple times each with one argument to pass multiple arguments to the Dart entrypoint. Currently this is only supported on desktop platforms.
-t, --target=<path>                                   The main entry-point file of the application, as run on the device.
                                                      If the --target option is omitted, but a file name is provided on the command line, then that is used instead.
                                                      (defaults to "lib\main.dart")
    --observatory-port                                (deprecated use host-vmservice-port instead) Listen to the given port for an observatory debugger connection.
                                                      Specifying port 0 (the default) will find a random free port.
                                                      Note: if the Dart Development Service (DDS) is enabled, this will not be the port of the Observatory instance advertised on the command line.
    --device-vmservice-port                           Look for vmservice connections only from the specified port.
                                                      Specifying port 0 (the default) will accept the first vmservice discovered.
    --host-vmservice-port                             When a device-side vmservice port is forwarded to a host-side port, use this value as the host port.
                                                      Specifying port 0 (the default) will find a random free host port.
    --[no-]pub                                        Whether to run "flutter pub get" before executing this command.
                                                      (defaults to on)
    --[no-]track-widget-creation                      Track widget creation locations. This enables features such as the widget inspector. This parameter is only functional in debug mode (i.e. when compiling JIT, not AOT).
                                                      (defaults to on)
    --[no-]null-assertions                            Perform additional null assertions on the boundaries of migrated and un-migrated code. This setting is not currently supported on desktop devices.
    --device-user=<10>                                Identifier number for a user or work profile on Android only. Run "adb shell pm list users" for available identifiers.
    --device-timeout=<10>                             Time in seconds to wait for devices to attach. Longer timeouts may be necessary for networked devices.
    --dds-port                                        When this value is provided, the Dart Development Service (DDS) will be bound to the provided port.
                                                      Specifying port 0 (the default) will find a random free port.
    --devtools-server-address                         When this value is provided, the Flutter tool will not spin up a new DevTools server instance, but instead will use the one provided at this address.
    --enable-software-rendering                       Enable rendering using the Skia software backend. This is useful when testing Flutter on emulators. By default, Flutter will attempt to either use OpenGL or Vulkan and fall back to software when neither is available.
    --skia-deterministic-rendering                    When combined with --enable-software-rendering, provides 100% deterministic Skia rendering.
    --[no-]await-first-frame-when-tracing             Whether to wait for the first frame when tracing startup ("--trace-startup"), or just dump the trace as soon as the application is running. The first frame is detected by looking for a Timeline event with the name "Rasterized first useful frame". By default, the widgets library's binding takes care of sending this event. 
                                                      (defaults to on)
    --[no-]use-test-fonts                             Enable (and default to) the "Ahem" font. This is a special font used in tests to remove any dependencies on the font metrics. It is enabled when you use "flutter test". Set this flag when running a test using "flutter run" for debugging purposes. This flag is only available when running in debug mode.
    --[no-]build                                      If necessary, build the app before running.
                                                      (defaults to on)
    --[no-]hot                                        Run with support for hot reloading. Only available for debug mode. Not available with "--trace-startup".
                                                      (defaults to on)
    --pid-file                                        Specify a file to write the process id to. You can send SIGUSR1 to trigger a hot reload and SIGUSR2 to trigger a hot restart.
    --[no-]fast-start                                 Whether to quickly bootstrap applications with a minimal app. Currently this is only supported on Android devices. This option cannot be paired with --use-application-binary.

Run "flutter help" to see global options.
