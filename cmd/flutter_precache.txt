Populate the Flutter tool's cache of binary artifacts.

If no explicit platform flags are provided, this command will download the artifacts for all currently enabled platforms

Global options:
-h, --help                  Print this usage information.
-v, --verbose               Noisy logging, including all shell commands executed.
                            If used with --help, shows hidden options.
-d, --device-id             Target device id or name (prefixes allowed).
    --version               Reports the version of this tool.
    --suppress-analytics    Suppress analytics reporting when this command runs.

Usage: flutter precache [arguments]
-h, --help              Print this usage information.
-a, --all-platforms     Precache artifacts for all host platforms.
-f, --force             Force re-downloading of artifacts.
    --[no-]android      Precache artifacts for Android development.
    --[no-]ios          Precache artifacts for iOS development.
    --[no-]web          Precache artifacts for web development.
    --[no-]linux        Precache artifacts for Linux desktop development.
    --[no-]windows      Precache artifacts for Windows desktop development.
    --[no-]macos        Precache artifacts for macOS desktop development.
    --[no-]fuchsia      Precache artifacts for Fuchsia development.
    --[no-]universal    Precache artifacts required for any development platform.
                        (defaults to on)

Run "flutter help" to see global options.
