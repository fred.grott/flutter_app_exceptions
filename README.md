# flutter_app_exceptions

![catch flutter applicaiton exceptions](./images/catch-flutter-application-exceptions.png)

A demo flutter app showing how to properly catch flutter app exceptions.

[**Explore the docs**](https://gitlab.com/fred.grott/flutter_app_exceptions)

<!-- Project Shields -->
![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/25023707?license=true&query=license.name&colorB=yellow)
![Created at](https://img.shields.io/badge/dynamic/json.svg?label=Created%20at&url=https://gitlab.com/api/v4/projects/25023707&query=created_at&colorB=informational)
![Last activity](https://img.shields.io/badge/dynamic/json.svg?label=Last%20activity&url=https://gitlab.com/api/v4/projects/25023707&query=last_activity_at&colorB=informational)
[![Contributors](https://badgen.net/gitlab/contributors/fred.grott/flutter_app_exceptions)](https://gitlab.com/fred.grott/flutter_app_exceptions/-/graphs/master)
[![Starrers](https://badgen.net/gitlab/stars/fred.grott/flutter_app_exceptions)](https://gitlab.com/fred.grott/flutter_app_exceptions/-/starrers)
[![Forks](https://badgen.net/gitlab/forks/fred.grott/flutter_app_exceptions)](https://gitlab.com/fred.grott/flutter_app_exceptions/-/forks)
[![Open Issues](https://badgen.net/gitlab/open-issues/fred.grott/flutter_app_exceptions)](https://gitlab.com/fred.grott/flutter_app_exceptions/-/issues)

<!-- TABLE OF CONTENTS gitlab does auto via their GFM one needs to add a toc tag onits own line-->


1. [flutter_app_exceptions](flutter_app_exceptions)
   1. [About The Project](#About The Project)
      1. [Built With](##Built With)
   2. [Getting Started](#Getting Started)
      1. [Prerequisites](##Prerequisities)
      2. [Installation](##Installation)
   3. [Usage](#Usage)
   4. [Roadmap](#Roadmap)
   5. [Contributing](#Contributing)
   6. [Contributors](#Contributors)
   7. [License](#License)
   8. [Contact](#Contact)
   9. [Acknowledgements](#Acknowledgements)
   10.[Resources](#Resources]

## About The Project

This is a demo flutter app project to demo how to properly handle and collect exception error data.

### Built With

- [Android Studio, Beta Edition](https://developer.android.com/studio/preview)

- [Microsoft's Visual Studio Code, Insiders Edition](https://code.visualstudio.com/insiders/)

- [Flutter Front-End Framework](https://flutter.dev/docs/get-started/install)

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisities

Most front-end application code requires usage with the git tool. To install git:

- [How To Install Git](https://git-scm.com/)

For Flutter Mobile Applications you need to install at least one mobile SDK:

- [How To Install Android SDK](https://anddroid.dev)

- [How To Install iOS SDK on MACOSX install XCope to get it](https://apps.apple.com/us/app/xcode/id497799835?mt=12)

Than install the Flutter SDK

- [How To Insall  the Flutter SDK](https://flutter.dev/docs/get-started/install.html)

Than install an IDE

- [How To install the Android IDE](https://developer.android.com/studio)

- [How To install the MS VSCode IDE](https://code.visualstudio.com/)

### Installation
1.  Clone the repo via _sh_

        git clone https://gitlab.com/fred.grott/beautiful_gitlab_readme.git


2.  Open the project in your Android Studio or MS VSCode IDE


## Usage

Clone and modify it to use in your own projects. The detailed article on how my Flutter Project SetUp works in the resources section of this readme.

## Roadmap

None, at the moment

## Contributing

Not accepting contriubtions at this as this is a prep project towards progression on my fluttter dev book chapters.

## Contributors


## License

Distributed under the BSD License. See [LICENSE](https://gitlab.com/fred.grott/flutter_app_exceptions/-/blob/master/LICENSE) for more information

## Contact

Fred Grott - [@twitter_handle](https://twitter.com/fredgrott) - email: fred DOT grott AT gmail DOT com

Project Link: 

[https://gitlab.com/fred.grott/flutter_app_exceptions](https://gitlab.com/fred.grott/flutter_app_exceptions)


## Acknowledgements

Thanks to the Google Flutter and Dart SDK teams for producing such easy to use sdk frameworks.

## Resources

- [Flutter Perfect Setup, article on how my base flutter_setup app code setup works which includes interation testing, code metrics, etc and published on CodeX which is a medium.com publication](https://medium.com/codex/flutter-perfect-setup-c5462b412f78)


